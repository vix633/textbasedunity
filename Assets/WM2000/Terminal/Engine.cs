﻿using System;
using cmdtestc.RoomObjects;
using cmdtestc.Domain;
using System.Collections.Generic;
using System.Linq;
using System.IO;
using UnityEngine;

namespace cmdtestc
{
    public class Engine
    {
        public Player m_player;
        IRoomLoader m_loader = new RoomLoader();
        public Dictionary<string, Room> m_rooms;
        public Action<string> m_terminal;
        public Engine()
        {
            m_rooms = m_loader.LoadRoomFromJson(Application.streamingAssetsPath+"/Static");
            m_player = new Player(m_rooms);
            Terminal.WriteLine(m_rooms.FirstOrDefault().Value.Greeting);
        }
        public void RouteCommand(string input)
        {
            string[] args = input.Split(' ');
            switch (args.Length)
            {
                case 1:
                    RouteVerbOnly(args[0]);
                    break;
                case 2:
                    RouteVerbAndContext(args[0], args[1]);
                    break;
                default:
                    Terminal.WriteLine($"I'm not sure what {input} means");
                    break;
            }
        }

        public void RouteVerbOnly(string verb)
        {
            switch (verb)
            {
                case nameof(VerbOnlyActions.inventory):
                    Terminal.WriteLine(m_player.GetInventory());
                    break;
                case nameof(VerbOnlyActions.location):
                    Terminal.WriteLine(m_player.GetLocation());
                    break;
                default:
                    string[] verbOnlyValues = Enum.GetNames(typeof(VerbOnlyActions));
                    string[] verbAndContextValues = Enum.GetNames(typeof(VerbAndContextActions));
                    Terminal.WriteLine($"Why not trying using one of the commands below: \n{String.Join(",", verbAndContextValues)},{String.Join(",", verbOnlyValues)}.\n" +
                    	"Some commands require a target, like:\n'look potato' \n'go door'\nor 'use rock:window'.");
                    break;
            }

        }
        public void RouteVerbAndContext(string verb, string context)
        {
            switch (verb)
            {
                case nameof(VerbAndContextActions.look):
                    Terminal.WriteLine(m_player.Look(context));
                    break;
                case nameof(VerbAndContextActions.go):
                    Terminal.WriteLine(m_player.Go(context));
                    break;
                case nameof(VerbAndContextActions.take):
                    Terminal.WriteLine(m_player.Take(context));
                    break;
                case nameof(VerbAndContextActions.use):
                    string[] commandAndArgs = context.Split(':');
                    Terminal.WriteLine(m_player.Use(commandAndArgs[0], commandAndArgs[1]));
                    break;
                default:
                    Terminal.WriteLine("Whatever you just did, didn't work");
                    break;

            }
        }
    }
}
