﻿using System;
using System.Collections.Generic;
using cmdtestc.RoomObjects;
using Newtonsoft.Json.Linq;

namespace cmdtestc
{
    [Serializable]
    public class Room
    {
        public string Greeting;
        public string Name;
        public string CompletionRule;
        public string CompletionText;
        public List<Item> Items;

    }
}
