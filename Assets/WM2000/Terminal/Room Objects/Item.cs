﻿using System;
using System.Collections.Generic;

namespace cmdtestc.RoomObjects
{
    [Serializable]
    public class Item
    {
        public string Id;
        public string Name;
        public string Text;
        public string Location;
        public string Interaction;
        public string InteractionText;
        public List<string> Storage;
        public bool CanTake;

    }
}
