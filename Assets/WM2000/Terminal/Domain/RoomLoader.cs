﻿using System;
using System.Collections.Generic;
using System.IO;
using cmdtestc.RoomObjects;
using Newtonsoft.Json;
using UnityEngine;

namespace cmdtestc
{
    public class RoomLoader : IRoomLoader
    {

        public Dictionary<string, Room> LoadRoomFromJson(string roomsPath = "./Assets/Static")
        {
            Dictionary<string, Room> rooms = new Dictionary<string, Room>();

            var room1 = Resources.Load<TextAsset>("Static/room1");
            var room2 = Resources.Load<TextAsset>("Static/room2");

            Room room11 = JsonUtility.FromJson<Room>(room1.text);
            Room room22 = JsonUtility.FromJson<Room>(room2.text);

            rooms.Add(room11.Name, room11);
            rooms.Add(room22.Name, room22);

            //string[] files = Directory.GetFiles(roomsPath);
            //foreach (var file in files)
            //{
            //    if (file.Contains(".meta")) { continue; }
            //    string roomFile = File.ReadAllText(file);
            //    Room room = JsonUtility.FromJson<Room>(roomFile);
            //    rooms.Add(room.Name, room);
            //}
            return rooms;

        }
    }
}
