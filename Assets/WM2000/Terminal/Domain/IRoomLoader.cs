﻿using System;
using System.Collections.Generic;

namespace cmdtestc
{
    public interface IRoomLoader
    {
        Dictionary<string, Room> LoadRoomFromJson(string path);
    }
}
