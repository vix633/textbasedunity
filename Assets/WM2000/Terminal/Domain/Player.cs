﻿using System;
using System.Collections.Generic;
using System.Linq;
using cmdtestc.RoomObjects;

namespace cmdtestc
{
    public class Player : IPlayer
    {
        public Room m_room { get; set; }
        public string m_location { get; set; }
        private List<Item> m_inventory { get; set; }
        private Dictionary<string, Room> m_rooms { get; set; }

        public Player(Dictionary<string, Room> rooms)
        {
            m_rooms = rooms;
            m_room = rooms.FirstOrDefault().Value;
            m_location = m_room.Name;
            m_inventory = new List<Item>();
        }

        public string GetLocation()
        {
            return $"You're currently located at: {m_location}";
        }

        public string GetInventory()
        {
            return $"You're holding {String.Join(",", m_inventory.Select((arg) => arg.Name))}";
        }

        public string Look(string itemOrLocation)
        {
            if (!m_room.Items.Exists((obj) => obj.Id == itemOrLocation))
            {
                return $"I can't find {itemOrLocation}.";
            }
            Item item = m_room.Items.Find((obj) => obj.Id == itemOrLocation);

            string returnText = $"{item.Text}...";
            if (item.Storage != null)
            {
                returnText += $"It contains the following items: \n {string.Join(",", item.Storage)}";
            }

            return returnText;
        }

        public string Take(string item)
        {
            Item foundItem = m_room.Items.Find((obj) => obj.Id == item);

            //TODO: Figure out how to take nested items with a location check;
            if (foundItem != null && foundItem.Location != m_location)
            {
                return $"You can't reach {item} from {m_location}";
            }
            if (foundItem != null & foundItem.CanTake)
            {
                Item storageLocation = m_room.Items.Find((obj) => obj.Id == foundItem.Location);
                foundItem.Location = null;
                m_inventory.Add(foundItem);
                m_room.Items.Remove(foundItem);
                storageLocation.Storage.Remove(foundItem.Id);
                return $"You take {item}";
            }
            return $"You you're having a hard time taking {item}";
        }

        public string Go(string location)
        {
            if (m_room.Items.Any((obj) => obj.Location == location))
            {
                m_location = location;
                return $"You go to {location}";
            }
            return $"You can't seem to get to {location}";
        }

        public string Use(string itemToUse, string itemToUseOn)
        {
            Item toBeUsedOn = m_room.Items.Find((obj) => obj.Id == itemToUseOn);

            if (CheckRoomCompletion(itemToUse, itemToUseOn))
            {

                string completionText = m_room.CompletionText;
                if (toBeUsedOn.Storage != null)
                {
                    MoveRooms(toBeUsedOn.Storage[0]);
                }
                string nextRoomGreeting = m_room.Greeting;
                m_location = m_room.Name;
                return $"{completionText} \n {nextRoomGreeting}";
            }


            Item toUse = m_inventory.Find((obj) => obj.Id == itemToUse);

            if (toUse == null)
            {
                return $"You dont have {itemToUse}";
            }

            if (toBeUsedOn.Location != m_location)
            {
                return $"You do your best to use {itemToUse} from where you are, but {toBeUsedOn.Location} is out of reach";
            }

            if (toBeUsedOn.Interaction == itemToUse)
            {
                //TODO: Multiple interactions between objects
                return toBeUsedOn.InteractionText;
            }
            return $"You use {itemToUse} on {itemToUseOn}, it's not very effective";
        }

        private bool CheckRoomCompletion(string itemToUse, string itemToUseOn)
        {
            string[] completionItems = m_room.CompletionRule.Split('/');
            if (completionItems.Contains(itemToUse) && completionItems.Contains(itemToUseOn))
            {
                return true;
            }
            return false;
        }
        private void MoveRooms(string roomName)
        {
            if (m_rooms.TryGetValue(roomName, out Room tempRoom))
            {
                m_room = tempRoom;
            }
        }
    }
}
