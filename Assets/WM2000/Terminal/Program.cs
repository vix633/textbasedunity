﻿using System;

namespace cmdtestc
{
    class Program
    {
        static void Main(string[] args)
        {
            Engine m_engine = new Engine();

            while (true)
            {
                m_engine.RouteCommand(Console.ReadLine());
            }
        }
    }
}
