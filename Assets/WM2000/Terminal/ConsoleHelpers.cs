using System;

static class ConsoleHelpers
{
static public void Write(string text, Action<string> writer){
        if(writer != null)
        {
            writer(text);
        }
        else
        {
            System.Console.WriteLine(text);
        }
    }
}
